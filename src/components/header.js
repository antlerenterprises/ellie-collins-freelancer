{/* Services */}
<Section>
<div className={`grid md:grid-cols-${3} gap-8 grid-cols-1`}>

  {
    [1, 2, 3].map((a, index) =>
      (<div className={`w-full bg-gray-300 py-6 px-5 text-center`}>
        <i className="fa fa-book text-2xl p-2 bg-gray-400 rounded-full"></i>
        <h2 className="tracking-widest text-left text-2xl mt-4 ">Service</h2>
        <p className="text-justify mb-4 ">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Expedita saepe aut harum doloribus ipsa quasi quidem est rem iusto sit, ex unde vel reprehenderit amet velit cum sunt perspiciatis nam.</p>
      </div>))
  }

</div>
</Section>

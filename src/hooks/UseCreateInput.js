import { TypesOfInput } from "../lib/Constants";


export default function(input) {
    const inputType = TypesOfInput[input.type.toLowerCase()]
    return inputType && inputType({...input, key: `${input.name}-${input.type}`})
}
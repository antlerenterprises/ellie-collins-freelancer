module.exports = {
  theme: {
    extend: {
      colors: {
        primary: 'var(--color-primary)',
        'primary-dark': 'var(--color-primary-dark)',
        secondary: 'var(--color-secondary)',
        'secondary-light': 'var(--color-secondary-light)',
        'dark-accent': 'var(--color-dark-accent)',
        accent: 'var(--color-accent)',
      },
      boxShadow: {
        '3xl': '0 25px 50px 4px rgba(0,0,0,.45)'
      }
    }
  },
  variants: {
    cursor: ['hover']
  },
  plugins: []
}
